<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/ROLEID)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-VERSION-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/PROJECTID)]() 
  [![License](https://img.shields.io/badge/license-LICENSE-blue.svg)](/LICENSE)

</div>

---

# Ansible System - Style

Makes style improvements to system.

It performs the following tasks:

- Installs fonts.
- **TODO:** Installs backgrounds.
- **TODO:** Installs icon sets.
  - **TODO:** Install Papirus Icon Theme.
  - **TODO:** Install Numix Circle Icon Theme.
  - **TODO:** Install Qogir Icon Theme.
  - **TODO:** Install vimix Icon Theme.
  - **TODO:** Install Obsidian Icon Theme.
  - **TODO:** Install Newaita Icon Theme.
- **TODO:** Installs gtk themes.
- **TODO:** Installs qt themes.

## 🦺 Requirements

*None*

## 🗃️ Role Variables

*TODO*

## 📦 Dependencies

*None*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

- Gerson Rojas <thegrojas@protonmail.com>
